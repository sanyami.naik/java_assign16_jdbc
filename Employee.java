package AssignmentsOne;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

public class Employee {

    Connection connection;
    Statement statement;
    PreparedStatement preparedStatement;
    ResultSet resultSet;

    Formatter formatter = new Formatter();

    int id;
    void setId(BufferedReader bufferedReader) throws IOException {
        System.out.println("Enter id");
        id=Integer.parseInt(bufferedReader.readLine());
    }


    public int getId() {
        return id;
    }

    void addRecords(BufferedReader bufferedReader) throws IOException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement=connection.createStatement();
            //statement.execute("Create table employee(id int,name varchar(20),designation varchar(20),dateOfJoining date,experience int,salary int,status varchar(20));");
            Employee employee=new Employee();
            System.out.println("Enter 10 records");
            for(int i=1;i<=10;i++)
            {
                preparedStatement=connection.prepareStatement("Insert into employee values(?,?,?,?,?,?,?);");
                employee.setId(bufferedReader);
                preparedStatement.setInt(1,employee.getId());
                System.out.println("Enter name");
                String name=bufferedReader.readLine();
                preparedStatement.setString(2,name);
                System.out.println("Enter designation");
                String designation= bufferedReader.readLine();
                preparedStatement.setString(3,designation);
                System.out.println("Enter the date of joining");
                String date= bufferedReader.readLine();
                preparedStatement.setString(4,date);
                System.out.println("Enter the number of experience");
                int experience=Integer.parseInt(bufferedReader.readLine());
                preparedStatement.setInt(5,experience);
                System.out.println("Enter the salary of the employee");
                int salary= Integer.parseInt(bufferedReader.readLine());
                preparedStatement.setInt(6,salary);
                System.out.println("Enter the status");
                String status=bufferedReader.readLine();
                preparedStatement.setString(7,status);


                preparedStatement.executeUpdate();
            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }



    void displayRecords()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("select * from employee;");


            resultSet = statement.executeQuery("select * from employee;");
            while (resultSet.next()) {

                formatter.format("%13s %13s %20s %15s %15s %15s %15s\n", "ID", "Name", "Designation", "DOJ", "EXP", "Salary", "Status");

                while (resultSet.next()){
                            formatter.format("%13s %13s %20s %15s %15s %15s %15s\n", resultSet.getInt(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5), resultSet.getInt(6), resultSet.getString(7));
                }
                System.out.println(formatter);
            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }



    void alterTable(BufferedReader bufferedReader) throws IOException {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement=connection.createStatement();
            System.out.println(statement.execute("Alter table employee add city varchar(20);"));
            resultSet=statement.executeQuery("Select name from employee;");
            while(resultSet.next())
            {
                System.out.println("name - "+resultSet.getString(1));
                String cityName=bufferedReader.readLine();
                statement=connection.createStatement();
                statement.execute("update employee set city='"+cityName+"' where name='"+resultSet.getString(1)+"';");
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    void conditionDisplay(BufferedReader bufferedReader) throws IOException {

        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement= connection.createStatement();

            resultSet=statement.executeQuery("select * from employee where salary>20000 && city='Pune';");


            while(resultSet.next()){
                System.out.println("name - "+resultSet.getString(2)+
                        "::salary-"+resultSet.getString(6)+"::city-"+resultSet.getString(8));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

    }




    void updateSalary()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement= connection.createStatement();
            statement.execute("Update employee set salary=salary+1000 where experience>3;");
            resultSet=statement.executeQuery("Select name,city,salary from employee;");

            while(resultSet.next()){
                System.out.println("name - "+resultSet.getString(1)+
                        "::city -"+resultSet.getString(2)+"::salary -"+resultSet.getString(3));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void deleteRecords()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments", "root","Root@12");
            statement= connection.createStatement();
            statement.execute("Delete from employee where status='left';");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}



/*OUTPUT:
1-Add 10 records
2-Show all records
3-Alter table by adding city
4-Fetch only those who stay in Pune and salary>20000
5-Update salary of employees whose experience is more than 3 year
6-Delete those who left
Enter your choice
2
           ID          Name          Designation             DOJ             EXP          Salary          Status
            3         disha             employee      2015-04-10               9            5300         working
            4         lasya              manager      2018-07-01               2            5000         working
            6       shivani             employee      2020-11-19               8          503000         manager
            7         viraj             employee      2020-04-18               5          103000         working
            8        naveen             employee      2017-04-19               1          160000         working
           10          neel              manager      2020-04-19               8          403000         working


1-Add 10 records
2-Show all records
3-Alter table by adding city
4-Fetch only those who stay in Pune and salary>20000
5-Update salary of employees whose experience is more than 3 year
6-Delete those who left
Enter your choice
5
name - sanyami::city -Mahad::salary -5000
name - disha::city -Pune::salary -6300
name - lasya::city -Mumbai::salary -5000
name - shivani::city -Pune::salary -504000
name - viraj::city -Pune::salary -104000
name - naveen::city -Delhi::salary -160000
name - neel::city -Mumbai::salary -404000
 */