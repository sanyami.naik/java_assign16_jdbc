package AssignmentsOne;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class StudentMenu {
    public static void main(String[] args) {

        Student student = new Student();
        Scanner scanner = new Scanner(System.in);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1-Adding column Gender");
        System.out.println("2-Show records with students in Ty and percentage >70");
        System.out.println("3-Records with id 1-10");
        System.out.println("4-3 operations in row");

        while(true)
        {
            System.out.println("Enter the option");
            int op=scanner.nextInt();

            switch(op){

                case 1:
                    student.createTable();
                    //student.addGenderColumn();
                    break;

                case 2:
                    student.Greaterthan70();
                    break;

                case 3:
                    student.inRangeOneToTen();
                    break;

                case 4:
                    student.add3();
                    break;

                case 10:
                    student.displayTable();
                    break;

        }


        }
    }
}
