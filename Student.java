package AssignmentsOne;

import java.sql.*;
import java.util.Formatter;

public class Student {

    Connection connection;
    Statement statement;
    ResultSet resultSet;
    PreparedStatement preparedStatement;
    Formatter formatter = new Formatter();

    void createTable(){
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");
            statement=connection.createStatement();
//            statement.execute("create table student(id int,name varchar(20),year varchar(40),percentage int,city varchar(20));");
//
//            statement.execute("Insert into student values (1,'Sanyami','FY',89,'Mahad');");
//            statement.execute("Insert into student  values (7,'Saloni','TY',90,'Pune');");
//            statement.execute("Insert into student values (4,'Ashraf','FY',69,'Mumbai');");
//            statement.execute("Insert into student values (9,'Praveer','SY',88,'Mahad');");
//            statement.execute("Insert into student values (3,'Raj','TY',58,'Pune');");
            statement.execute("Insert into student values (89,'Pravin','SY',88,'Mahad','M');");
            statement.execute("Insert into student values (45,'Ayushi','TY',58,'Pune','F');");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void addGenderColumn()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");
            statement=connection.createStatement();
            statement.execute("Alter table student add column gender varchar(10);");
            statement.execute("update student set gender='F' where id= 1 or id=7;");
            statement.execute("update student set gender='M' where id= 4 or id=9 or id=3;");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void displayTable()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("Select * from student;");
            formatter.format("%13s %13s %20s %15s %15s %15s \n", "ID", "Name", "Year", "Percentage", "City", "Gender");

            while(resultSet.next())
            {
                formatter.format("%13s %13s %20s %15s %15s %15s \n",resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6));
            }

            System.out.println(formatter);

        } catch (SQLException e) {
            System.out.println(e);
        }

    }


    void Greaterthan70()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("Select * from student where percentage>70 and year='TY';");
            formatter.format("%13s %13s %20s %15s %15s %15s \n", "ID", "Name", "Year", "Percentage", "City", "Gender");

            while(resultSet.next())
            {
                formatter.format("%13s %13s %20s %15s %15s %15s \n",resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6));
            }

            System.out.println(formatter);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void inRangeOneToTen()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("Select * from student where id in(1,10);");


            resultSet=statement.executeQuery("Select * from student where percentage>70 and year='TY';");
            formatter.format("%13s %13s %20s %15s %15s %15s \n", "ID", "Name", "Year", "Percentage", "City", "Gender");

            while(resultSet.next())
            {
                formatter.format("%13s %13s %20s %15s %15s %15s \n",resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getInt(4),resultSet.getString(5),resultSet.getString(6));
            }

            System.out.println(formatter);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void add3()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/assignments","root","Root@12");

            preparedStatement=connection.prepareStatement("Select * from student;");
            preparedStatement.execute();
            preparedStatement.addBatch("Insert into student values(89,'Pravin','SY',93,'Mahad','M');");
            preparedStatement.addBatch("Insert into student values(90,'Devyani','TY',68,'Mahad','F');");
            preparedStatement.addBatch("Insert into student values(91,'NILIN','FY',80,'Mahad','M');");
            preparedStatement.addBatch("update student set name= (select concat('Mr. ',name)) where gender='M';");
            preparedStatement.addBatch("update student set name= (select concat('Mrs. ',name)) where gender='F';");
            preparedStatement.addBatch("Delete from student where year='TY';");

            preparedStatement.executeBatch();



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


}



/*output
-Adding column Gender
2-Show records with students in Ty and percentage >70
3-Records with id 1-10
4-3 operations in row
Enter the option
1
Enter the option
2
           ID          Name                 Year      Percentage            City          Gender
            7        Saloni                   TY              90            Pune               F

Enter the option
3
           ID          Name                 Year      Percentage            City          Gender
            7        Saloni                   TY              90            Pune               F
           ID          Name                 Year      Percentage            City          Gender
            7        Saloni                   TY              90            Pune               F

Enter the option
4
Enter the option
10
           ID          Name                 Year      Percentage            City          Gender
            7        Saloni                   TY              90            Pune               F
           ID          Name                 Year      Percentage            City          Gender
            7        Saloni                   TY              90            Pune               F
           ID          Name                 Year      Percentage            City          Gender
            1  Mrs. Sanyami                   FY              89           Mahad               F
            4    Mr. Ashraf                   FY              69          Mumbai               M
            9   Mr. Praveer                   SY              88           Mahad               M
           89    Mr. Pravin                   SY              88           Mahad               M
           89    Mr. Pravin                   SY              93           Mahad               M
           91     Mr. NILIN                   FY              80           Mahad               M

 */