package AssignmentsOne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class EmployeeMenu {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1-Add 10 records");
        System.out.println("2-Show all records");
        System.out.println("3-Alter table by adding city");
        System.out.println("4-Fetch only those who stay in Pune and salary>20000");
        System.out.println("5-Update salary of employees whose experience is more than 3 year");
        System.out.println("6-Delete those who left");

        Employee employee=new Employee();

        while (true) {
            System.out.println("Enter your choice");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    employee.addRecords(bufferedReader);
                    break;

                case 2:
                    employee.displayRecords();
                    break;

                case 3:
                    employee.alterTable(bufferedReader);
                    break;

                case 4:
                    employee.conditionDisplay(bufferedReader);
                    break;

                case 5:
                    employee.updateSalary();
                    break;

                case 6:
                    employee.deleteRecords();
                    break;
                case 7:
                    System.exit(0);
                    break;

                default:
                    System.out.println("Enter valid choice");;
                    break;


            }
        }
    }
}











